"""Import and export routines for Gmsh."""


_nodes_per_elem_type = {1:2,
                        2:3,
                        3:4,
                        4:4,
                        5:8,
                        6:6,
                        7:5,
                        8:3,
                        9:6,
                        10:9,
                        11:10,
                        12:27,
                        13:18,
                        14:14,
                        15:1,
                        16:8,
                        17:20,
                        18:15,
                        19:13,
                        20:9,
                        21:10,
                        22:12,
                        23:15,
                        24:15,
                        25:21,
                        26:4,
                        27:5,
                        28:6,
                        29:20,
                        30:35,
                        31:56,
                        92:64,
                        93:125}

def parse_gmsh(file_name):
    """Import an ascii based Gmsh file."""
    from bempp_converter.core import Grid
    import numpy as np

    def read_version(s):
        """Read the Gmsh file version."""
        tokens = s.split()
        if len(tokens) != 3:
            raise ValueError("File header has unsupported format.")
        try:
            version = float(tokens[0])
        except:
            raise ValueError("Version number not recognized.")
        return version

    def read_vertex(s):
        """Read a vertex."""
        tokens = s.split()
        if len(tokens) != 4:
            raise ValueError(
                "Unsupported format for vertex in string {0}".format(s))
        try:
            index = int(tokens[0])
            x = float(tokens[1])
            y = float(tokens[2])
            z = float(tokens[3])
        except:
            raise ValueError("Vertex value not recognized in string %s", s)

        return index, x, y, z


    def read_element(s):
        """Read an element."""
        tokens = s.split()
        try:
            index = int(tokens[0])
            elem_type = int(tokens[1])
        except:
            raise ValueError(
                "Unspported format for element in string %s", s)
        if elem_type != 2:
            return None
        try:
            phys_id = int(tokens[3])
            v2 = int(tokens[-1])
            v1 = int(tokens[-2])
            v0 = int(tokens[-3])
        except:
            raise ValueError(
                "Unsupported format for element in string {0}".format(s))
        return index, v0, v1, v2, phys_id

    all_vertices = None
    all_vertex_ids = None
    elements_list = None
    elements_ids_list = None
    domain_indices_list = None

    binary = False
    data_size = None
    from IPython import embed

    with open(file_name, 'rb') as f:
        while True:
            s = f.readline().rstrip().decode('utf-8')
            if s == '':
                break
            if s == "$MeshFormat":
                s = f.readline().rstrip().decode('utf-8')
                tokens = s.split()
                if tokens[0] != "2.2":
                    raise ValueError("Only Gmsh 2.2 file format supported.")
                if int(tokens[1]) == 1:
                    binary = True
                data_size = int(tokens[2])
                if binary:
                    # Read endian line but do not process it.
                    # Always assume little endian (Intel type machines)
                    s = f.readline().rstrip().decode('utf-8')
                #pylint: disable=protected-access
                s = f.readline().rstrip().decode('utf-8')
                if not s == "$EndMeshFormat":
                    raise ValueError(
                        "Expected $EndMeshFormat but got {0}".format(s))
                continue
            if s == "$Nodes":
                s = f.readline().rstrip().decode('utf-8')
                try:
                    number_of_vertices = int(s)
                except:
                    raise ValueError("Expected integer, got {0}".format(s))

                all_vertices = np.zeros((3, number_of_vertices), dtype='float64')
                all_vertex_ids = np.zeros(number_of_vertices, dtype='uint32')

                if binary:
                    n = number_of_vertices * (4 + 3 * data_size)
                    s = f.read(n)
                    dtype = [('id', np.int32), ('vertex', np.float64, (3,))]
                    vertex_data = np.fromstring(s, dtype=dtype)
                    all_vertices[:,:] = vertex_data['vertex'].T
                    all_vertex_ids[:] = vertex_data['id']
                    s = f.readline().rstrip().decode('utf-8') # Read end of line
                    s = f.readline().rstrip().decode('utf-8') # Read EndNodes
                else:
                    count = 0
                    s = f.readline().rstrip()
                    while s != "$EndNodes":
                        index, x, y, z = read_vertex(s)
                        all_vertices[:, count] = (x, y, z)
                        all_vertex_ids[count] = index
                        count += 1
                        s = f.readline().rstrip().decode('utf-8')
                        if count == number_of_vertices:
                            break
                    if count != number_of_vertices:
                        raise ValueError(
                            "Expected %i vertices but got %i vertices.",
                            number_of_vertices, count)
                if s != "$EndNodes":
                    raise ValueError(
                        "Expected $EndNodes but got %s.", s)
            if s == "$Elements":
                s = f.readline().rstrip().decode('utf-8')
                try:
                    number_of_elements = int(s)
                except:
                    raise ValueError("Expected integer, got %s", s)
                elements_list = []
                element_ids_list = []
                domain_indices_list = []

                if binary:
                    import struct
                    count = 0
                    while count < number_of_elements:
                        elem_header = struct.unpack('iii',f.read(3 * 4))
                        elem_type = elem_header[0]
                        nelements = elem_header[1]
                        ntags = elem_header[2]
                        nnodes = _nodes_per_elem_type[elem_type]
                        for _ in range(nelements):
                            index = struct.unpack('i', f.read(4))[0]
                            tags = struct.unpack(ntags * 'i', f.read(4 * ntags))
                            nodes = struct.unpack(nnodes * 'i', f.read(4 * nnodes))
                            if elem_type == 2:
                                element_ids_list.append(index)
                                elements_list.append(nodes)
                                domain_indices_list.append(tags[0])
                            count += 1
                    # Get the readline at the end of the elements
                    s = f.readline().rstrip().decode('utf-8')
                    # Read EndElements
                    s = f.readline().rstrip().decode('utf-8')
                else:
                    count = 0
                    s = f.readline().rstrip().decode('utf-8')
                    while s != "$EndElements":
                        elem = read_element(s)
                        if elem is not None:
                            index, v1, v2, v3, phys_id = elem
                            elements_list.append([v1, v2, v3])
                            domain_indices_list.append(phys_id)
                            element_ids_list.append(index)
                        count += 1
                        s = f.readline().rstrip().decode('utf-8')
                        if count == number_of_elements:
                            break
                    if count != number_of_elements:
                        raise ValueError(
                            "Expected %i elements but got %i elements.",
                            number_of_elements, count)
                if s != "$EndElements":
                    raise ValueError(
                        "Expected $EndElements but got %s.", s)
        return (all_vertices, all_vertex_ids, 
                elements_list, element_ids_list,
                domain_indices_list)


def export_data_sets(file_name, grid_data_sets, description='',
        binary=True):
    """
    Export data sets to Gmsh.

    For each data set a new Gmsh file is created and named
    consecutively file_name +'_0', file_name + '1', etc.

    """
    import struct

    nsets = len(grid_data_sets)
    if nsets == 1:
        extensions = ['']
    elif nsets > 1:
        extensions = [str(index) for index in range(nsets)]
    else:
        raise ValueError(
            "'grid_data_sets' must be a list with at least one element.")

    for data_set_index, data_set in enumerate(grid_data_sets):
        with open(file_name + extensions[data_set_index], 'wb') as f:
            f.write('$MeshFormat\n'.encode('utf-8'))
            if binary:
                f.write("2.2 1 8\n".encode('utf-8'))
                f.write(struct.pack('i',1))
                f.write('\n'.encode('utf-8'))
            else:
                f.write("2.2 0 8\n".encode('utf-8'))
            f.write('$EndMeshFormat\n'.encode('utf-8'))
            f.write('$Nodes\n'.encode('utf-8'))
            nvertices = data_set.grid.number_of_vertices
            vertices = data_set.grid.vertices
            nvertices_str = str(nvertices) + '\n'
            f.write(nvertices_str.encode('utf-8'))
            for index in range(1, nvertices+1):
                if binary:
                    f.write(struct.pack("iddd", index,
                            vertices[0, index-1],
                            vertices[1, index-1],
                            vertices[2, index-1]))
                else:
                    f.write("{0} {1} {2} {3}\n".format(
                        index,
                        vertices[0, index-1],
                        vertices[1, index-1],
                        vertices[2, index-1]).encode('utf-8'))
            if binary:
                f.write("\n".encode('utf-8'))
            f.write("$EndNodes\n".encode('utf-8'))




            










def create_grid_structure(all_vertices, all_vertex_ids,
        elements_list, element_ids_list, domain_indices_list):
    """Take the parsed Gmsh data, and create the grid structure."""
    from bempp_converter.core import Grid
    import numpy as np

    # We need to sort through the vertices and elements and only
    # keep those vertices that are associated with the boundary
    # elements

    vertex_used = -1 * np.ones(all_vertices.shape[1], dtype='int')
    elements = np.zeros((3, len(elements_list)), dtype='uint32')
    vertex_count = 0
    for element_index, elem in enumerate(elements_list):
        for vertex_index, vertex in enumerate(elem):
            if vertex_used[vertex-1] == -1:
                vertex_used[vertex-1] = vertex_count
                vertex_count += 1
            elements[vertex_index, element_index] = vertex_used[vertex-1]
    vertices = np.zeros((3, vertex_count), dtype='float64')

    # Now choose the vertices in the right order
    # np.argsort reverses a permutation

    index_set = np.argsort(vertex_used[np.where(vertex_used>-1)])
    vertices[:,:] = all_vertices[:, index_set]
    vertex_ids = np.zeros(vertex_count, dtype='uint32')
    vertex_ids[:] = all_vertex_ids[index_set]
    domain_indices = np.array(domain_indices_list, dtype='uint32')
    element_ids = np.array(element_ids_list, dtype='uint32')
    return Grid(vertices, elements, vertex_ids, element_ids,
            '', domain_indices)
                 

def import_data_sets(file_name):
    """Import Gmsh data sets"""
    from bempp_converter.core import GridDataSet
    (all_vertices, all_vertex_ids, 
            elements_list, element_ids_list, domain_indices_list) = \
                    parse_gmsh(file_name)
    grid_structure = create_grid_structure(all_vertices, all_vertex_ids,
            elements_list, element_ids_list, domain_indices_list)
    data_dict = {'grid_data_sets': [GridDataSet(grid_structure)]}
    return data_dict






                

    return vertices, elements, vertex_ids, element_ids, domain_indices
